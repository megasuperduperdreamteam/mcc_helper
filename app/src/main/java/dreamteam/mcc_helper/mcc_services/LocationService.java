package dreamteam.mcc_helper.mcc_services;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import dreamteam.mcc_helper.Constants;
import dreamteam.mcc_helper.R;

/**
 *  Location Service
 */

class LocationService {

    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private String TAG = LocationService.class.getSimpleName();
    private Context context;

    // Initializer
    LocationService(Context context) {
        this.context = context;
        Log.i(TAG,"Start LocationService");
        buildLocationRequest();
        buildFusedLocationProviderClient();
        buildLocationCallback();
        startLocationUpdates();
    }


    // Recognizing device location coordinates
    private void buildFusedLocationProviderClient(){
        Log.i(TAG,"Start buildFusedLocationProviderClient");
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {

                        if (location.hasAltitude()) {
                            Log.i(TAG,String.valueOf(location.getAltitude()));

                        } else {
                            Log.i(TAG,"No altitude available");
                        }
                        if (location.hasSpeed()) {
                            Log.i(TAG,String.valueOf(location.getSpeed()) + "m/s");
                        } else {
                            Log.i(TAG,"No speed available");
                        }

                    }
                }
            });
        }
    }

    // Updating location coordinates
    private void startLocationUpdates() {
        Log.i(TAG,"Start startLocationUpdates");
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
        }
    }


    // If location coordinates was modified this method return Latitude, Longitude, Accuracy
    private void buildLocationCallback(){
        Log.i(TAG,"Start buildLocationCallback");
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                for (Location location : locationResult.getLocations()) {
                    //Update UI with location data
                    if (location != null) {

                        Log.i(TAG,">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n"+
                                        "Latitude -> "+String.valueOf(location.getLatitude()) + "\n" +
                                        "Longitude-> "+String.valueOf(location.getLongitude()) + "\n" +
                                        "Accuracy -> "+String.valueOf(location.getAccuracy()) + "\n");
                        if (location.hasAltitude()) {
                            Log.i(TAG,String.valueOf(location.getAltitude()));
                        } else {
                            Log.i(TAG,"No altitude available");
                        }
                        if (location.hasSpeed()) {
                            Log.i(TAG,String.valueOf(location.getSpeed()) + "m/s");
                        } else {
                            Log.i(TAG,"No speed available");
                        }
                        Log.i(TAG,">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                    }
                }
            }
        };
    }

    // Set  parameters to location request
    private void buildLocationRequest(){
        Log.i(TAG,"Start buildLocationRequest");
        locationRequest = new LocationRequest();
        locationRequest.setInterval(Constants.LOCATION_REQUEST_INTERVAL); //use a value fo about 10 to 15s for a real app
        locationRequest.setFastestInterval(Constants.LOCATION_REQUEST_FASTEST_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setSmallestDisplacement(Constants.LOCATION_REQUEST_SMALLEST_DISPLACEMENT);

    }
}
