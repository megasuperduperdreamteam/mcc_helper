package dreamteam.mcc_helper.mcc_services;


import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import android.os.Build;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;

import android.util.Log;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import dreamteam.mcc_helper.Constants;

import dreamteam.mcc_helper.R;
import dreamteam.mcc_helper.db_parser.ShPreferencesHelper;
import dreamteam.mcc_helper.mcc_parser.AsyncParser;
import dreamteam.mcc_helper.mcc_parser.GeoZone;
import dreamteam.mcc_helper.mcc_parser.Schedule;
import dreamteam.mcc_helper.mcc_parser.Station;

import static dreamteam.mcc_helper.Constants.GEOFENCE_ON_ENTER;
import static dreamteam.mcc_helper.Constants.GEOFENCE_ON_EXIT;
import static dreamteam.mcc_helper.Constants.GEOFENCE_REMOVE;
import static dreamteam.mcc_helper.Constants.NOTIFICATION_BUTTON;
import static dreamteam.mcc_helper.Constants.NOTIFICATION_REFRESH;

/**
 * Main Service
 */

public class MainService extends Service {

    private List<String> removeList;
    private List<Station> allStations;
    private List<GeoZone> allGlobalGeozones;
    private long exitStationTime = 0;
    private boolean networkStatus;
    private GeofenceService geofenceService;
    private LocationService locationService;
    private String TAG = MainService.class.getSimpleName();

    // Intent message handler from GeofenceTransitionsIntentService for recognizing incomming commands
    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG,"Intent getAction -> "+intent.getAction());

            // If we lost internet connection we can tap on notification and try again to get shedule

            if (Objects.equals(intent.getAction(), Constants.NOTIFICATION_BUTTON)) {
                String stationName = intent.getStringExtra(getString(R.string.retry));
                Log.i(TAG, "We get RETRY MESSAGE");
                Log.i(TAG, "Starting retrying notification");
                try {
                    createStationNotification(stationName);
                } catch (NullPointerException e) {

                    NotificationService.errorNotification(getApplicationContext(), stationName);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }

            // Removing stations geofence when we leave one of the global geofence

            if (Objects.equals(intent.getAction(), Constants.GEOFENCE_REMOVE)) {
                String removeMessage = intent.getStringExtra(getString(R.string.removeStations));
                Log.i(TAG, "We get REMOVE STATION MESSAGE");
                Log.i(TAG, "Starting remove stations");
                if (!removeMessage.isEmpty()) {
                    try {
                        geofenceService.removeFromGeofenceClient();
                    } catch (Exception e){
                        NotificationService.globalErrorNotification(context, e);
                        e.printStackTrace();
                        System.exit(0);
                    }
                }
            }

            // Adding new stations geofence when we entered at global geofence

            if (Objects.equals(intent.getAction(), Constants.GEOFENCE_UPDATES)) {
                Log.i(TAG, "We get GEOZONE ENTERED MESSAGE");
                String message = intent.getStringExtra(context.getString(R.string.zone));
                Log.i(TAG, "We enter geozone -> "+message);
                Integer zone_id = Integer.valueOf(message);
                try {
                    geofenceService.myStartStationGeofences(zone_id);
                } catch (Exception e) {
                    NotificationService.globalErrorNotification(context, e);
                    e.printStackTrace();
                    System.exit(0);
                }

            }

            // Checking internet connection and creating notification when we entered at station geofence

            if (Objects.equals(intent.getAction(), Constants.GEOFENCE_ON_ENTER)) {
                Log.i(TAG, "We get STATION ENTERED MESSAGE");
                if ((exitStationTime == 0) || (System.currentTimeMillis() > exitStationTime)){
                    String message = intent.getStringExtra(getString(R.string.createNotification));
                    networkStatus = NotificationService.isOnline(context);
                    try {
                        if (networkStatus){
                            createStationNotification(message);
                        } else {
                            throw new NullPointerException(getString(R.string.connectionException));
                        }

                    } catch (ExecutionException | InterruptedException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        Log.i(TAG, "At first case, No connection we retrying error notification");
                        NotificationService.errorNotification(context,message);
                        e.printStackTrace();
                    }
                }

            }

            // If we pressed refresh button this will update notification

            if (Objects.equals(intent.getAction(),Constants.NOTIFICATION_REFRESH)) {
                Log.i(TAG, "We get REFRESH MESSAGE");
                String message = intent.getStringExtra(getString(R.string.createNotification));
                networkStatus = NotificationService.isOnline(context);
                try {
                    if (networkStatus) {
                        createStationNotification(message);
                    } else {
                        throw new NullPointerException(getString(R.string.connectionException));
                    }
                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    Log.i(TAG, "At second case, No connection we retrying error notification");
                    NotificationService.errorNotification(context,message);
                    e.printStackTrace();
                }


            }

            // set timer when we leave station

            if (Objects.equals(intent.getAction(),Constants.GEOFENCE_ON_EXIT)) {
                Log.i(TAG, "We get EXIT STATION MESSAGE");
                exitStationTime = System.currentTimeMillis() + Constants.MOVE_TIME;
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "onCreate MainService");
        allStations = ShPreferencesHelper.getAllStationsInfo(this);
        allGlobalGeozones = ShPreferencesHelper.getAllGeoFenceInfo(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Starting MainService");

        locationService = new LocationService(this);
        NotificationService.createNotifyManager(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter(Constants.GEOFENCE_UPDATES));
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter(GEOFENCE_REMOVE));
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter(GEOFENCE_ON_ENTER));
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter(GEOFENCE_ON_EXIT));
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter(NOTIFICATION_REFRESH));
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter(NOTIFICATION_BUTTON));

        if (Objects.equals(intent.getAction(), Constants.NOTIFICATION_BUTTON)) {
            String stationName = intent.getStringExtra(getString(R.string.retry));
            if (stationName != null) {
                Log.i(TAG, "Starting retrying notification");
                try {
                    createStationNotification(stationName);
                } catch (NullPointerException e) {

                    NotificationService.errorNotification(this,stationName);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }

        } else {

            try {
                geofenceService = new GeofenceService(this, allGlobalGeozones, allStations);
                removeList = new ArrayList<>();

                this.startForeground(Constants.FOREGROUND_NOTIFICATION_CHANNEL_ID, NotificationService.createForegroundNotify(this, this));

            } catch (Exception e) {
                NotificationService.globalErrorNotification(this,e);
                System.exit(0);
            }

            return START_STICKY_COMPATIBILITY;
        }
        return START_STICKY_COMPATIBILITY;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void createStationNotification(String stationName) throws ExecutionException, InterruptedException {
        AsyncParser mt = new AsyncParser(getApplicationContext(), stationName);
        mt.execute();
        Schedule schedule = mt.get();
        NotificationService.createStationNotification(this, stationName, schedule);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
