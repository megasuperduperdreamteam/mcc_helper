package dreamteam.mcc_helper.mcc_services;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.List;

import dreamteam.mcc_helper.Constants;
import dreamteam.mcc_helper.GeofenceTransitionsIntentService;
import dreamteam.mcc_helper.R;
import dreamteam.mcc_helper.mcc_parser.GeoZone;
import dreamteam.mcc_helper.mcc_parser.Station;

/**
 *  Geofence Service
 */

public class GeofenceService {

    private GeofencingClient mGeofencingClient;
    private PendingIntent mGeofencePendingIntent;
    private String TAG = GeofenceService.class.getSimpleName();
    private List<String> removeList;
    private GeofencingRequest listOfGeofences;
    private List<Station> allStations;
    private List<GeoZone> allGlobalGeozones;
    private boolean geofenceListCleaned = false;
    private Context context;

    // Geofence service initialiser
    GeofenceService(Context context,  List<GeoZone> allGlobalGeozones, List<Station> allStations) {
        this.context = context;
        Log.i(TAG, "Start GeofenceService");
        this.allGlobalGeozones = allGlobalGeozones;
        this.allStations = allStations;
        this.removeList = new ArrayList<>();
        mGeofencingClient = LocationServices.getGeofencingClient(context);
        mGeofencePendingIntent = getGeofencePendingIntent();
        GeofencingRequest myGeofenceRequest = getGeofencingRequest(createGlobalGeofence());
        addGeofence(myGeofenceRequest);
    }


    // Remove station geofence from list of current global geofence
    public void removeFromGeofenceClient () throws Exception{
        Log.i(TAG, "Start removeFromGeofenceClient");
        Log.i(TAG,"RemoveSize List ->" + removeList.size());

        if (!geofenceListCleaned){
            if (removeList.size() != 0){
                mGeofencingClient.removeGeofences(removeList);
                Log.i(TAG, "Printing old geofenceRequest -> "+ listOfGeofences.toString());
                removeList.clear();
            }
            geofenceListCleaned = true;
        }else{
            geofenceListCleaned = false;
        }
    }


    // Cleaning removeList, creating station geofence and adding it to GeofenceClient
    protected void myStartStationGeofences(Integer zone) throws Exception {
        Log.i(TAG, "StartStation Geofences (Start Monitoring stations)");
        removeFromGeofenceClient();
        GeofencingRequest myGeofenceRequest = getGeofencingRequest(createStationGeofence(zone));
        addGeofence(myGeofenceRequest);
    }



    // Adding geofences to: remove list and to GeofencingClient
    private void addGeofence(GeofencingRequest myGeofenceRequest) {
        Log.i(TAG, "addGeofence");
        listOfGeofences = myGeofenceRequest;
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "We get permission to get location");
            Log.i(TAG, "Our GeofenceRequest -> " + myGeofenceRequest.toString());
            Log.i(TAG,"Our GeofencePendingIntent -> " +  mGeofencePendingIntent.toString());

            mGeofencingClient.addGeofences(myGeofenceRequest, mGeofencePendingIntent)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.i(TAG, "Add geofence success");
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.i(TAG, "Failed to add listOfGeofences " + e.getMessage());
                        }
                    });
        }

    }

    // Creating station geofence
    private ArrayList<Geofence> createStationGeofence(Integer zone_id){
        Log.i(TAG, "Start ADD STATIONS:");
        ArrayList<Geofence> mGeofenceList = new ArrayList<>();

        for (Station station: allStations){
            if (station.getZone().equals(zone_id)) {
                Log.i(TAG, "Need Station -> "+ station.getName() + "  Zone -> "+ station.getZone() );
                removeList.add(station.getName());
                mGeofenceList.add(new Geofence.Builder()
                        // Set the request ID of the geofence. This is a string to identify this
                        // geofence.
                        .setRequestId(station.getName())

                        .setCircularRegion(
                                station.getLatitude(), station.getLongitude(),

                                Constants.GEOFENCE_STATION_RADIUS_IN_METERS
                        )
                        .setExpirationDuration(Geofence.NEVER_EXPIRE)
                        .setLoiteringDelay(Constants.LOITERING_DELAY)
                        .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                                Geofence.GEOFENCE_TRANSITION_DWELL |
                                Geofence.GEOFENCE_TRANSITION_EXIT)
                        .build());
            }
        }

        return mGeofenceList;
    }

    // Creating global geofence
    private ArrayList<Geofence> createGlobalGeofence(){
        Log.i(TAG, "Start creating global Geofences");

        ArrayList<Geofence> mGeofenceList = new ArrayList<>();

        for (GeoZone zone: allGlobalGeozones){
            Log.i(TAG, "Zone -> "+ zone.getId());
            mGeofenceList.add(new Geofence.Builder()
                    // Set the request ID of the geofence. This is a string to identify this
                    // geofence.
                    .setRequestId(String.valueOf(zone.getId()))

                    .setCircularRegion(
                            zone.getLatitude(), zone.getLongitude(),

                            zone.getRadius()
                    )
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .setLoiteringDelay(Constants.LOITERING_DELAY)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                            Geofence.GEOFENCE_TRANSITION_DWELL |
                            Geofence.GEOFENCE_TRANSITION_EXIT)
                    .build());

        }
        return mGeofenceList;
    }

    // Updating information about location inside geofence
    private PendingIntent getGeofencePendingIntent() {
        Log.i(TAG, "Get GeofencePendingIntent");
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        GeofenceTransitionsIntentService intentService = new GeofenceTransitionsIntentService();
        Intent intent = new Intent(context, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    // Creating geofence request with initial trigger event - "ENTER"
    private GeofencingRequest getGeofencingRequest(ArrayList<Geofence> mGeofenceList) {
        Log.i(TAG, " getGeofencingRequest");
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }
}
