package dreamteam.mcc_helper.mcc_services;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.widget.Toast;


import dreamteam.mcc_helper.mcc_parser.Schedule;
import dreamteam.mcc_helper.mcc_parser.Train_time;
import java.util.List;
import java.util.concurrent.ExecutionException;
import dreamteam.mcc_helper.Constants;
import dreamteam.mcc_helper.R;
import dreamteam.mcc_helper.StationActivity;
import dreamteam.mcc_helper.SubClasses;

/**
 *  Notification service
 */

public class NotificationService {

    private static NotificationManager notificationManager;
    private  static String TAG = NotificationService.class.getSimpleName();

    // Notification manager
    public static void createNotifyManager(Context context) {
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.i(TAG,"NotificationManger starts");
            NotificationChannel channel = new NotificationChannel(Constants.NOTIFICATION_CHANEL_STRING_ID, Constants.NOTIFICATION_CHANEL_NAME, NotificationManager.IMPORTANCE_LOW);
            channel.setDescription(context.getString(R.string.notificationManagerDescription));
            channel.enableVibration(false);
            channel.enableLights(false);
            channel.setLightColor(Color.GREEN);
            notificationManager.createNotificationChannel(channel);
        }
    }

    // Station notification with short shedule
    @RequiresApi(api = Build.VERSION_CODES.O)
    public static void createStationNotification(Context context, String stationName, Schedule schedule) throws ExecutionException, InterruptedException {
        Intent resultIntent = new Intent(context, StationActivity.class);

        List<Train_time> test = SubClasses.get_closest_timetable_time(schedule.getClockwise_timetable());
        Log.i(TAG, test.toString());
        Train_time time_clockwise_first = test.get(0);
        Train_time time_clockwise_second = test.get(1);

        test = SubClasses.get_closest_timetable_time(schedule.getCounterclockwise_timetable());
        Train_time time_counterclockwise_first = test.get(0);
        Train_time time_counterclockwise_second = test.get(1);

        resultIntent.putExtra(context.getString(R.string.timetableClockwise), schedule.getClockwise_timetable());
        resultIntent.putExtra(context.getString(R.string.timetableCounterclockwise), schedule.getCounterclockwise_timetable());


        SpannableString spannableStringClockwise = new SpannableString(schedule.getClockwise_timetable().getComment());
        spannableStringClockwise.setSpan(new StyleSpan(Typeface.BOLD),0,schedule.getClockwise_timetable().getComment().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        SpannableString spannableStringCounterClockwise = new SpannableString(schedule.getCounterclockwise_timetable().getComment());
        spannableStringCounterClockwise.setSpan(new StyleSpan(Typeface.BOLD),0,schedule.getCounterclockwise_timetable().getComment().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
        inboxStyle.setBigContentTitle(context.getString(R.string.shortInfo) + " " + stationName);
        inboxStyle.addLine(spannableStringClockwise);
        inboxStyle.addLine(context.getString(R.string.nearestTrain) + " " + SubClasses.cast_time_to_str(time_clockwise_first.getTime()));
        inboxStyle.addLine(context.getString(R.string.nextTtrain) + " " + SubClasses.cast_time_to_str(time_clockwise_second.getTime()));

        inboxStyle.addLine(spannableStringCounterClockwise);
        inboxStyle.addLine(context.getString(R.string.nearestTrain) + " " + SubClasses.cast_time_to_str(time_counterclockwise_first.getTime()));
        inboxStyle.addLine(context.getString(R.string.nextTtrain) + " " + SubClasses.cast_time_to_str(time_counterclockwise_second.getTime()));

        PendingIntent resultPendingIntent = PendingIntent.getActivities(context, 0, new Intent[]{resultIntent}, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Builder notifyBuilder = new Notification.Builder(context, Constants.NOTIFICATION_CHANEL_STRING_ID)
                .setStyle(inboxStyle)
                .setSmallIcon(R.drawable.ic_add2)
                .setContentTitle(stationName)
                .setColor(72626)
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent);
        Notification notification = notifyBuilder.build();
        notificationManager.notify(Constants.STATION_NOTIFICATION_CHANNEL_ID, notification);
    }

    // Foreground notification. Taping will call system settings with application information
    public static Notification createForegroundNotify (Context context, Service service) throws Exception{

        Intent resultIntent = new Intent();
        resultIntent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts(context.getString(R.string.pckg), context.getPackageName(), null);
        resultIntent.setData(uri);

        PendingIntent resultPendingIntent = PendingIntent.getActivities(service, 0, new Intent[]{resultIntent}, PendingIntent.FLAG_UPDATE_CURRENT);


        Notification.Builder notifBuilder = new Notification.Builder(service,Constants.NOTIFICATION_CHANEL_STRING_ID)
                .setContentTitle(context.getString(R.string.app_name) + context.getString(R.string.titleMessage))
                .setContentText(context.getString(R.string.notifTextMessage))
                .setSmallIcon(R.drawable.ic_add2)
                .setOngoing(false)
                .setColor(72626)
                .setTicker(context.getString(R.string.notifTicker))
                .setContentIntent(resultPendingIntent);
        return notifBuilder.build();
    }

    // Checking internet connection
    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = null;
        if (cm != null) {
            netInfo = cm.getActiveNetworkInfo();
        }
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    // Error notification when internet connection is lost. Taping by notification will recall checking method
    public static void  errorNotification(Context context,String stationName) {

        Intent intent = new Intent(context, MainService.class);
        intent.setAction(Constants.NOTIFICATION_BUTTON);

        intent.putExtra(context.getString(R.string.retry), stationName);
        PendingIntent contentIntent = PendingIntent.getService(context, 0, intent, 0);

        Notification.Builder notifBuilder = new Notification.Builder(context, Constants.NOTIFICATION_CHANEL_STRING_ID)
                .setSmallIcon(R.drawable.ic_add2)
                .setContentTitle(context.getString(R.string.errorTitle))
                .setContentText(context.getString(R.string.errorTextMessage))
                .setColor(72626)
                .setContentIntent(contentIntent)
                .setAutoCancel(true);
        Notification notification = notifBuilder.build();
        notificationManager.notify(Constants.STATION_NOTIFICATION_CHANNEL_ID, notification);
    }


    // Error handler
    public static void globalErrorNotification(Context context,Exception e) {
        Notification.Builder notifBuilder = new Notification.Builder(context, Constants.NOTIFICATION_CHANEL_STRING_ID)
                .setSmallIcon(R.drawable.ic_add2)
                .setContentTitle(context.getString(R.string.error))
                .setStyle(new Notification.BigTextStyle().bigText(e.getMessage()))
                .setColor(72626)
                .setAutoCancel(true);
        Notification notification = notifBuilder.build();
        notificationManager.notify(Constants.STATION_NOTIFICATION_CHANNEL_ID, notification);
    }
}
