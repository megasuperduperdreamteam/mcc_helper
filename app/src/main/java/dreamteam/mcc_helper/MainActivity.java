package dreamteam.mcc_helper;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.SearchView;
import android.widget.Toast;

import dreamteam.mcc_helper.db_parser.ShPreferencesHelper;
import dreamteam.mcc_helper.mcc_adapter.MccRecyclerAdapter;
import dreamteam.mcc_helper.mcc_adapter.RecyclerItemClickListener;
import dreamteam.mcc_helper.mcc_parser.Station;
import dreamteam.mcc_helper.mcc_services.MainService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;
import static dreamteam.mcc_helper.Constants.GEOFENCE_ON_ENTER;
import static dreamteam.mcc_helper.Constants.GEOFENCE_ON_EXIT;
import static dreamteam.mcc_helper.Constants.PERMISSION_REQUEST_FINE_LOCATION;

/**
 *  Main Activity
 */

public class MainActivity extends AppCompatActivity {

    private RecyclerView stations_listview;
    private List<Station> allStations;
    private List<Station> allStationsCopy;
    private Intent activityIntent;
    private  String TAG = MainActivity.class.getSimpleName();
    private FloatingActionButton updateButton;
    private String stationEntered;
    private int buttonFlag = 0;

    // Receiver of GeofenceTransitionsIntentService messages/ Controlling refresh button
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onReceive(Context context, Intent intent) {

            Log.i(TAG,"Receive intent action in MainActivity -> "+intent.getAction());

            if (Objects.equals(intent.getAction(), Constants.GEOFENCE_ON_ENTER)) {
                if (buttonFlag==0){
                    updateButton.setVisibility(View.VISIBLE);
                    stationEntered = intent.getStringExtra(context.getString(R.string.createNotification));
                    buttonFlag++;
                }else if (buttonFlag >= 1){
                    buttonFlag++;
                    stationEntered = intent.getStringExtra(context.getString(R.string.createNotification));
                }
                Log.i(TAG,"Button iterator plus->"+buttonFlag);
            }else if (Objects.equals(intent.getAction(), Constants.GEOFENCE_ON_EXIT)) {
                if (buttonFlag==1){
                    updateButton.setVisibility(View.GONE);
                    buttonFlag--;
                } else if (buttonFlag >=1){
                    buttonFlag--;
                }
                Log.i(TAG,"Button iterator minus->"+buttonFlag);
            }
        }
    };

    // Start point
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(TAG,"Start onCreate MainActivity");

        stations_listview = findViewById(R.id.stations_list);
        SearchView searchView = (SearchView) findViewById(R.id.searchView);
        updateButton = (FloatingActionButton) findViewById(R.id.fab);
        ShPreferencesHelper.initialize(getApplicationContext());
        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_FINE_LOCATION);

        allStations = ShPreferencesHelper.getAllStationsInfo(getApplicationContext());
        allStationsCopy = new ArrayList<>(allStations); // Copy our Array for search optimization
        final MccRecyclerAdapter adapter = new MccRecyclerAdapter(allStations,allStationsCopy);
        stations_listview.setAdapter(adapter);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        stations_listview.setLayoutManager(llm);

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter(GEOFENCE_ON_ENTER));
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter(GEOFENCE_ON_EXIT));

        final Animation animation = new RotateAnimation(0.0f, 360.0f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, //Animation of restart button
                0.5f);
        animation.setRepeatCount(5);
        animation.setDuration(1000);

        updateButton.setVisibility(View.GONE);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Constants.NOTIFICATION_REFRESH);
                intent.putExtra(getString(R.string.createNotification), stationEntered);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                updateButton.startAnimation(animation);
            }
        });

        Log.i(TAG, "Trying to start MainService");
        Intent intent = new Intent(this, MainService.class); //Start service
        startForegroundService(intent);
        Log.i(TAG, "Start MainService");

        adapter.updateData(allStations,allStationsCopy);
        adapter.notifyDataSetChanged();

        // Searching field with filters

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                adapter.filter(s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.filter(s);
                return false;
            }
        });


        // Set StationActivity as handler of recyclerView click

        activityIntent = new Intent(this, StationActivity.class);
        stations_listview.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), stations_listview ,new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                        Log.i(TAG, "RecyclerView: itemClick, position = " + position);
                        Log.i(TAG, "RecyclerView: stationName is "+allStations.get(position).getName());

                        activityIntent.putExtra(getString(R.string.stationName), allStations.get(position).getName());
                        startActivity(activityIntent);

                    }

                    @Override public void onLongItemClick(View view, int position) {
                    }
                })
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "OnResume start");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause start");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy start");
    }


    //Receive Result Handler
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSION_REQUEST_FINE_LOCATION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "Permission granted");
                } else {
                    Log.i(TAG, "Permission is not granted");
                    Toast.makeText(getApplicationContext(), R.string.toastMessage, Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
        }
    }

}
