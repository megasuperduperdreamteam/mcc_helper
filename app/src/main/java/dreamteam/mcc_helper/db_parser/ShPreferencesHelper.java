package dreamteam.mcc_helper.db_parser;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.List;

import dreamteam.mcc_helper.Constants;
import dreamteam.mcc_helper.mcc_parser.GeoZone;
import dreamteam.mcc_helper.mcc_parser.Station;

import static android.content.Context.MODE_PRIVATE;

/**
 *  Class for getting information about geofences from SharedPreferences
 **/

public class ShPreferencesHelper {

    private static String DB_PATH = "";
    private static String TAG = ShPreferencesHelper.class.getSimpleName();
    private static final int DATABASE_VERSION = 3;
    private static final String SP_KEY_DB_VER = "db_ver";


    //Initialization of sharedPreference file.
    public static void initialize(Context context) {
        String parentPath = context.getCacheDir().getParent();
        DB_PATH = parentPath + "/shared_prefs";

        Log.i(TAG, "Initialise db");
        if (databaseExists(context, DB_PATH)) { //if we have existing ShPref file
            Log.i(TAG, "Db is already exists");
            SharedPreferences prefs = PreferenceManager
                    .getDefaultSharedPreferences(context);
            int dbVersion = prefs.getInt(SP_KEY_DB_VER, 1);
            if (DATABASE_VERSION != dbVersion) { //if we have newer project version
                Log.i(TAG, "Found old db - try to update");
                if (!deleteDb(context, DB_PATH)) {
                    Log.w(TAG, "Unable to update database");
                }
                Log.i(TAG, "Successful delete old db");
            }
        }
        if (!databaseExists(context, DB_PATH)) {
            createDatabase(context);
        }
    }


    private static void writeData(Context context, String fileName, String key, String data){
        SharedPreferences.Editor editor = context.getSharedPreferences(fileName, MODE_PRIVATE).edit();
        editor.putString(key, data);
        editor.apply();
    }

    private static  String readData(Context context, String fileName, String key){
        SharedPreferences prefs  = context.getSharedPreferences(fileName, MODE_PRIVATE);
        return  prefs.getString(key, null);
    }




    //Get info about all Stations
    public static List<Station> getAllStationsInfo(Context context) {
        Type stationType = new TypeToken<List<Station>>(){}.getType();
        Gson gson = new Gson();
        return gson.fromJson(ShPreferencesHelper.readData(context, Constants.MY_PREFS_STATIONS_FILE, "Stations"), stationType);
    }


    //Get info about all GeoZones
    public static  List<GeoZone> getAllGeoFenceInfo(Context context) {
        Type geozoneType = new TypeToken<List<GeoZone>>(){}.getType();
        Gson gson = new Gson();
        return gson.fromJson(ShPreferencesHelper.readData(context, Constants.MY_PREFS_GEOZONES_FILE, "Geozones"), geozoneType);
    }



    //Check if sharedPref files exist
    private static  boolean databaseExists(Context context, String path) {
        File dbFileStations = new File(path +"/"+Constants.MY_PREFS_STATIONS_FILE_XML);
        File dbFileGeozones = new File(path +"/"+Constants.MY_PREFS_STATIONS_FILE_XML);
        return dbFileStations.exists() && dbFileGeozones.exists();
    }



    //Creates database by copying it from assets directory.
    private static void createDatabase(Context context) {
        Log.i(TAG, "Main method -> createDatabase");
        String parentPath = context.getCacheDir().getParent();
        DB_PATH = parentPath + "/shared_prefs";
        Log.i(TAG, DB_PATH);

        File file = new File(parentPath);
        if (!file.exists()) {
            if (!file.mkdir()) {
                Log.w(TAG, "Unable to create database directory");
                return;
            }
        }
        File need_dir = new File(DB_PATH);
        if (!need_dir.exists()){
            if (!need_dir.mkdir())
            {
                    Log.w(TAG, "Unable to create database directory with ShPref");
                    return;
            }
        }

        InputStream isStation = null;
        OutputStream osStation = null;

        InputStream isZone = null;
        OutputStream osZone = null;
        try {

            isStation = context.getAssets().open(Constants.MY_PREFS_STATIONS_FILE_XML);
            osStation = new FileOutputStream(DB_PATH+"/"+Constants.MY_PREFS_STATIONS_FILE_XML);
            copyFile(isStation, osStation);

            isZone = context.getAssets().open(Constants.MY_PREFS_GEOZONES_FILE_XML);
            osZone = new FileOutputStream(DB_PATH+"/"+Constants.MY_PREFS_GEOZONES_FILE_XML);
            copyFile(isZone, osZone);

            SharedPreferences prefs = PreferenceManager
                    .getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt(SP_KEY_DB_VER, DATABASE_VERSION);
            editor.apply();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (isStation != null) {
                try {
                    isStation.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (osStation != null) {
                try {
                    osStation.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (isZone != null) {
                try {
                    isZone.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (osZone != null) {
                try {
                    osZone.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void copyFile(InputStream is, OutputStream os) throws IOException {
        byte[] buffer = new byte[1024];
        int length;
        while ((length = is.read(buffer)) > 0) {
            os.write(buffer, 0, length);
        }
        os.flush();
    }

    private static boolean deleteDb(Context context, String path){
        File dbFileStations = new File(path +"/"+Constants.MY_PREFS_STATIONS_FILE_XML);
        File dbFileGeozones = new File(path +"/"+Constants.MY_PREFS_STATIONS_FILE_XML);
        return dbFileStations.delete() && dbFileGeozones.delete();
    }
}
