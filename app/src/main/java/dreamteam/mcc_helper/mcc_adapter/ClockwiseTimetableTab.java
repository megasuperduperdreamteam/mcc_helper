package dreamteam.mcc_helper.mcc_adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import dreamteam.mcc_helper.R;
import dreamteam.mcc_helper.SubClasses;
import dreamteam.mcc_helper.mcc_parser.Timetable;
import dreamteam.mcc_helper.mcc_parser.Train_time;

import static dreamteam.mcc_helper.SubClasses.get_current_time;

/**
 *  TimetableTab for adding in Fragments.
 *  We could use one TimetableTab class but we had plans for these classes
 **/

public class ClockwiseTimetableTab extends Fragment {

    private Timetable timetable;
    public TableLayout tableLayout;
    public ScrollView clockwise_sv;
    public Integer scrollRow;
    Context context;

    public ClockwiseTimetableTab() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setTimetable(Timetable timetable, Context context) {
       this.timetable = timetable;
       this.context = context;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_clockwise_timetable, container, false);

        tableLayout = (TableLayout) view.findViewById(R.id.clockwise_table);
        clockwise_sv = (ScrollView) view.findViewById(R.id.scroll_clockwise);

        TableRow row;

        TextView text1;
        TextView text2;
        TextView text3;
        TableRow.LayoutParams param = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, (float) 1.0);

        boolean flag = false;
        for (Train_time train_time:timetable.getTable().values()) {
            row = new TableRow(context);
            text1 = new TextView(context);
            text2 = new TextView(context);
            text3 = new TextView(context);


            text1.setLayoutParams(param);
            text2.setLayoutParams(param);
            text3.setLayoutParams(param);

            text1.setGravity(Gravity.CENTER);
            text2.setGravity(Gravity.CENTER);
            text3.setGravity(Gravity.CENTER);

            text1.setPadding(15, 15, 15, 15);
            text2.setPadding(15, 15, 15, 15);
            text3.setPadding(15, 15, 15, 15);


            text1.setText( SubClasses.cast_time_to_str(train_time.getTime()));
            row.addView(text1);
            text2.setText(train_time.getComment());
            row.addView(text2);
            text3.setText(train_time.getStops());
            row.addView(text3);
            tableLayout.addView(row);
            if ((!flag)&&train_time.getTime().after(get_current_time())) {
                flag = true;
                scrollRow = tableLayout.indexOfChild(row);
            }

        }

        return view;
    }


}
