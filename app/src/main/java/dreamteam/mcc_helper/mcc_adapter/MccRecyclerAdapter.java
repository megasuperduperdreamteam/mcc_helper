package dreamteam.mcc_helper.mcc_adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

import dreamteam.mcc_helper.R;
import dreamteam.mcc_helper.mcc_parser.Station;
/**
 *  Adapter for RecyclerView
 **/

public class MccRecyclerAdapter extends RecyclerView.Adapter<MccRecyclerAdapter.ViewHolder>{

    private List<Station> stationList;
    private List<Station> stationListCopy;

    public MccRecyclerAdapter(List<Station> stationList, List<Station> stationListCopy) {
        this.stationList = stationList;
        this.stationListCopy = stationListCopy;
    }


    public void updateData(List<Station> stationList, List<Station> stationListCopy) {
        this.stationList = stationList;
        this.stationListCopy = stationListCopy;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.station_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.stationName.setText(stationList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return stationList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView stationName;
        ViewHolder(View itemView) {
            super(itemView);
            stationName = (TextView) itemView.findViewById(R.id.stationName);

        }
    }


    public void filter(String text) {
        stationList.clear();
        if(text.isEmpty()){
            stationList.addAll(stationListCopy);
        } else{
            text = text.toLowerCase();
            for(Station item: stationListCopy){
                if(item.getName().toLowerCase().contains(text) ){
                    stationList.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }

}
