package dreamteam.mcc_helper.mcc_parser;

import android.content.Context;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import dreamteam.mcc_helper.db_parser.ShPreferencesHelper;

/**
 *  Class which parsing official web-site of mcc timetable
 **/

public class Parser {

    private static String TAG = Parser.class.getSimpleName();

    public static Schedule get_station_schedule(String station, Context context) throws IOException, InterruptedException { //Entry point
        return parse_schedule(Objects.requireNonNull(getStation(station, context)));
    }

    private static Schedule parse_schedule(Station station) throws RuntimeException, IOException, InterruptedException { //Parse timetable site of certain station and return result as Schedule Object
        Log.i(TAG, "Parse Schedule start");
        Document doc = Jsoup.connect(station.getUrl()).get();
        Elements elem = doc.getElementsByClass("b-timetable-direction").select("div.b-timetable-direction__cell:lt(2)");
        TimetableThread ThreadClockwise = new TimetableThread(station.getUrl()+elem.get(0).select("a").attr("href"),elem.get(0).text());   //getting url to Clockwise timetable
        TimetableThread ThreadCounterclockwise = new TimetableThread(station.getUrl()+elem.get(1).select("a").attr("href"),elem.get(1).text()); //getting url to Counterclockwise timetable
        Thread TechThread1 =  new Thread(ThreadClockwise);  //starting thread to parse Clockwise timetable
        Thread TechThread2 =  new Thread(ThreadCounterclockwise);  //starting thread to parse  Counterclockwise timetable
        TechThread1.start();
        TechThread2.start();
        TechThread1.join();
        TechThread2.join();
        Timetable clockwise_timetable;
        Timetable counterclockwise_timetable;
        if (station.getReverse()){
            counterclockwise_timetable = ThreadClockwise.getValue();
            clockwise_timetable = ThreadCounterclockwise.getValue();
        }else {
            clockwise_timetable = ThreadClockwise.getValue();
            counterclockwise_timetable = ThreadCounterclockwise.getValue();
        }
        Log.i(TAG, "Parse Schedule end");
        return  new Schedule(station.getName(), station.getUrl(), clockwise_timetable, counterclockwise_timetable); //Return Schedule Object
    }


    private static Station getStation(String station, Context context){ //Get from ShPref file info about certain Station

        List<Station> allStations= ShPreferencesHelper.getAllStationsInfo(context);
        for (Station buffer: allStations){
            if (station.equals(buffer.getName())){
                return buffer;
            }
        }
        return null;
    }




}
