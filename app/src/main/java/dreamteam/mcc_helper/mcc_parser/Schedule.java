package dreamteam.mcc_helper.mcc_parser;

/**
 *  Class which parsing official web-site of mcc timetable
 **/

public class Schedule {

    private String station;
    private String url;
    private Timetable clockwise_timetable;
    private Timetable counterclockwise_timetable;

    Schedule(String station, String url, Timetable clockwise_timetable, Timetable counterclockwise_timetable) {
        this.station = station;
        this.url = url;
        this.clockwise_timetable = clockwise_timetable;
        this.counterclockwise_timetable = counterclockwise_timetable;
    }

    public String getStation() {
        return station;
    }

    private String getUrl() {
        return url;
    }

    public Timetable getClockwise_timetable() {
        return clockwise_timetable;
    }

    public Timetable getCounterclockwise_timetable() {
        return counterclockwise_timetable;
    }
}
