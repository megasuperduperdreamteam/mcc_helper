package dreamteam.mcc_helper.mcc_parser;

/**
 *  Class describing the GeoZone
 *  GeoZone - an entity that unites several small geofences.
 **/

public class GeoZone {

    private Integer id;
    private Float latitude;
    private Float longitude;
    private Float radius;

    public GeoZone(Integer id, Float latitude, Float longitude, Float radius) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;
    }

    public Integer getId() {
        return id;
    }

    public Float getLatitude() {
        return latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public Float getRadius() {
        return radius;
    }
}
