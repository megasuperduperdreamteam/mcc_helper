package dreamteam.mcc_helper.mcc_parser;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;

/**
 *  AsyncTask for background operation - parsing official mcc timetable site
 **/

public class AsyncParser extends AsyncTask<Void, Void, Schedule> {

    private String TAG = AsyncParser.class.getSimpleName();;
    private Context context;
    private String station;


    public AsyncParser(Context context, String station) {
        this.context = context;
        this.station = station;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.i(TAG, "Start AsyncTAsk");
    }

    @Override
    protected Schedule doInBackground(Void... params) {
        Log.i(TAG, "Do AsyncTAsk");
        Schedule result = null;

        try {
            result = Parser.get_station_schedule(station,context); //Start parsing timetable of certain station
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onPostExecute(Schedule result) {
        super.onPostExecute(result);
        Log.i(TAG, "End AsyncTAsk");
    }
}