package dreamteam.mcc_helper.mcc_parser;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *  Class TimetableThread - thread for parsing html doc into Timetable class
 **/

public class TimetableThread implements Runnable{

    private String TAG = TimetableThread.class.getSimpleName();;
    private String url;
    private String comment;
    private Timetable timetable;

    TimetableThread(String url, String comment) {
        this.url = url;
        this.comment = comment;
    }

    @Override
    public void run() {
        parse_timetable();
    }

    public Timetable getValue() {
        return timetable;
    }

    private  Timetable parse_timetable(){
        Log.i(TAG, "parse_timetable_Start");
        timetable = null;
        try {
            Document doc = Jsoup.connect(url).get();
            Elements elem = doc.getElementsByClass("b-timetable").select("tr.b-timetable__row");
            Map<Time,Train_time> time_line = new LinkedHashMap<>();
            DateFormat formatter = new SimpleDateFormat("HH:mm");

            for (Element element : elem){
                String select_time = element.select("td:eq(1)").text();
                String select_comment = element.select("td:eq(0)").text();
                String select_stops = element.select("td:eq(2)").text();

                Time parse_time = new Time(formatter.parse(select_time.split(",")[0]).getTime());
                String parse_comment = "";
                if (select_comment.contains("(")) {
                    parse_comment = select_comment.substring(select_comment.indexOf("(") + 1, select_comment.indexOf(")")).trim();
                }else{
                    parse_comment = "Без ограничений";
                }
                String parse_stops = select_stops.trim();

                Train_time train_time = new Train_time(parse_time, parse_comment, parse_stops); //Train_time - our object for keeping information about arriving train
                time_line.put(parse_time, train_time);

            }

            timetable = new Timetable(time_line,comment);
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "parse_timetable_Stop");
        return timetable;

    }
}


