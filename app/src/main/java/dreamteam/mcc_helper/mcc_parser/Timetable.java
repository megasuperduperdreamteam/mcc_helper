package dreamteam.mcc_helper.mcc_parser;

import android.os.Parcel;
import android.os.Parcelable;

import java.sql.Time;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *  Class describing Timetable
 *  Timetable is the part of Schedule class.
 **/

public class Timetable  implements Parcelable {

    private Map<Time, Train_time> table;
    private String comment ;

    private Timetable(Parcel in) {
        table = new LinkedHashMap<>();
        in.readMap(table,Train_time.class.getClassLoader());
        comment = in.readString();
    }

    Timetable(Map<Time, Train_time> table, String comment) {
        this.table = table;
        this.comment = comment;
    }

    public static final Creator<Timetable> CREATOR = new Creator<Timetable>() {
        @Override
        public Timetable createFromParcel(Parcel in) {
            return new Timetable(in);
        }

        @Override
        public Timetable[] newArray(int size) {
            return new Timetable[size];
        }
    };

    public Map<Time, Train_time> getTable() {
        return table;
    }

    public String getComment() {
        return comment;
    }

    public void put(Time key, Train_time value) {
        this.table.put(key,value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeMap(table);
        parcel.writeString(comment);
    }
}

