package dreamteam.mcc_helper.mcc_parser;

import android.os.Parcel;
import android.os.Parcelable;

import java.sql.Time;

/**
 *  Class Train_Time - class for keeping arrival time (time), train limits (comment), train stops (stops)
 **/

public class Train_time implements Parcelable{

    private Time time;
    private String comment;
    private String stops;

    Train_time(Time time, String comment, String stops) {
        this.time = time;
        this.comment = comment;
        this.stops = stops;
    }

    //method to make our class serializable
    private Train_time(Parcel in) {
        time = Time.valueOf(in.readString());
        comment = in.readString();
        stops = in.readString();

    }

    public static final Creator<Train_time> CREATOR = new Creator<Train_time>() {
        @Override
        public Train_time createFromParcel(Parcel in) {
            return new Train_time(in);
        }

        @Override
        public Train_time[] newArray(int size) {
            return new Train_time[size];
        }
    };

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStops() {
        return stops;
    }

    public void setStops(String stops) {
        this.stops = stops;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(String.valueOf(time));
        parcel.writeString(comment);
        parcel.writeString(stops);
    }
}
