package dreamteam.mcc_helper.mcc_parser;

/**
 *  Class describing Station
 **/

public class Station {

    private String name;
    private Float latitude;
    private Float longitude;
    private String url;
    private  Boolean reverse;
    private  Integer zone;

    public Station(String name, Float latitude, Float longitude, String url, Boolean reverse, Integer zone) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.url = url;
        this.reverse = reverse;
        this.zone = zone;
    }

    public Integer getZone() {
        return zone;
    }

    public String getName() {
        return name;
    }

    public Float getLatitude() {
        return latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public String getUrl() {
        return url;
    }

    public Boolean getReverse() {
        return reverse;
    }

}
