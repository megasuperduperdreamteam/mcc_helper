package dreamteam.mcc_helper;

public class Constants {
    public final static int GEOFENCE_STATION_RADIUS_IN_METERS = 300;

    public final static int LOITERING_DELAY = 10000;
    public final static int LOCATION_REQUEST_INTERVAL = 0;//7500;
    public final static int LOCATION_REQUEST_FASTEST_INTERVAL = 0;//5000;
    public final static int LOCATION_REQUEST_BIG_INTERVAL = 15000;
    public final static int LOCATION_REQUEST_BIG_FASTEST_INTERVAL = 10000;
    public final static int LOCATION_REQUEST_SMALLEST_DISPLACEMENT = 300;

    public final static int STATION_NOTIFICATION_CHANNEL_ID = 3;
    public final static int NOTIFICATION_CHANNEL_ID = 2;
    public final static int FOREGROUND_NOTIFICATION_CHANNEL_ID = 1;
    public final static String NOTIFICATION_CHANEL_STRING_ID = "2";
    public final static String NOTIFICATION_CHANEL_NAME = "MY_CHANEL";


    public final static long MOVE_TIME = 900000; //3000  //900000;
    public final static int PERMISSION_REQUEST_FINE_LOCATION = 101;

    public final static String GEOFENCE_UPDATES = "dreamteam.mcc_helper.GeofenceUpdates";
    public final static String GEOFENCE_REMOVE = "dreamteam.mcc_helper.GeofenceRemove";
    public final static String GEOFENCE_ON_ENTER = "dreamteam.mcc_helper.onEnter";
    public final static String GEOFENCE_ON_EXIT = "dreamteam.mcc_helper.onExit";
    public final static String NOTIFICATION_REFRESH = "dreamteam.mcc_helper.Refresh";
    public final static String NOTIFICATION_BUTTON = "dreamteam.mcc_helper.Button";

    public static final String MY_PREFS_STATIONS_FILE_XML = "MyPrefsStations.xml";
    public static final String MY_PREFS_STATIONS_FILE = "MyPrefsStations";
    public static final String MY_PREFS_GEOZONES_FILE_XML = "MyPrefsGeozones.xml";
    public static final String MY_PREFS_GEOZONES_FILE = "MyPrefsGeozones";

}
