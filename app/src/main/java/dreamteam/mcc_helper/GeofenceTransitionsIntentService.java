package dreamteam.mcc_helper;


import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import static dreamteam.mcc_helper.Constants.GEOFENCE_ON_ENTER;
import static dreamteam.mcc_helper.Constants.GEOFENCE_ON_EXIT;
import static dreamteam.mcc_helper.Constants.GEOFENCE_REMOVE;
import static dreamteam.mcc_helper.Constants.GEOFENCE_UPDATES;
import java.util.ArrayList;
import java.util.List;

/**
 * Geofence Transitions Intent Service
 */

public class GeofenceTransitionsIntentService extends IntentService {

    private String TAG = GeofenceTransitionsIntentService.class.getSimpleName();


    @Override
    public void onCreate() {
        super.onCreate();
    }

    public GeofenceTransitionsIntentService() {
        super(GeofenceTransitionsIntentService.class.getName());
        Log.i(TAG, "IntentService Constructor");
    }


    // Recognition geofence events
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG, "onHandleIntent");
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);

        if (geofencingEvent.hasError()) {
            String errorMessage = getString(R.string.errorString) + geofencingEvent.getErrorCode();
            Log.i(TAG, errorMessage);
            return;
        }

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();
        Log.i(TAG, "Handle without error\n Geofence Event Type is " + geofenceTransition);

        // Test that the reported transition was of interest.

        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER || geofenceTransition == Geofence.GEOFENCE_TRANSITION_DWELL ||
                geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {

            // Get the listOfGeofences that were triggered. A single event can trigger
            // multiple listOfGeofences.
            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();
            Log.i(TAG, "Triggering listOfGeofences: "+ triggeringGeofences.toString());

            if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
                Log.i(TAG,"We enter listOfGeofences ->" + triggeringGeofences.toString());
                Log.i(TAG,"Length list of entered listOfGeofences ->" + triggeringGeofences.size());

                for (Geofence geofence:triggeringGeofences){

                    if (Character.isDigit(geofence.getRequestId().charAt(0))) {
                        Log.i(TAG,"We entered GeoZone " + geofence.getRequestId());
                        sendMessageToActivity(this,geofence.getRequestId());
                    } else if (!Character.isDigit(geofence.getRequestId().charAt(0))) {
                        Log.i(TAG,"We entered Stations geofence " + geofence.getRequestId());
                        sendNotificationMessageToActivity(this,geofence.getRequestId());
                    }
                }

            }

            if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT ) {
                Log.i(TAG,"We exit listOfGeofences ->" + triggeringGeofences.toString());
                Log.i(TAG,"Length list of exited listOfGeofences ->" + triggeringGeofences.size());

                for (Geofence geofence:triggeringGeofences) {

                    if (Character.isDigit(geofence.getRequestId().charAt(0))) {
                        Log.i(TAG, "We exit GeoZone " + geofence.getRequestId());
                        sendRemoveMessageToActivity(this, geofence.getRequestId());
                        sendExitMessageToActivity(this, getString(R.string.exit));
                    } else if (!Character.isDigit(geofence.getRequestId().charAt(0))) {
                        Log.i(TAG, "We exit Stations geofence " + geofence.getRequestId());
                        sendExitMessageToActivity(this, getString(R.string.exit));
                    }
                }
            }

            // Get the transition details as a String.
            String geofenceTransitionDetails = getGeofenceTransitionDetails(
                    this,
                    geofenceTransition,
                    triggeringGeofences
            );

            // Send notification and log the transition details.
            Log.i(TAG, "⇊⇊All geofence information here⇊⇊");
            Log.i(TAG, geofenceTransitionDetails);

        } else {
            // Log the error.
            Log.i(TAG, "Geofence ERROR");
            Log.i(TAG, "Geofence transition invalid type");
        }
    }

    String getGeofenceTransitionDetails(Context context, int geofenceTransition, List<Geofence> triggeringGeofences)
    {   Log.i(TAG, "getGeofenceTransitionDetails");
        String geofenceTransitionString = getTransitionString(geofenceTransition);

        ArrayList<String> triggeringGeofencesIdsList = new ArrayList<>();
        for (Geofence geofence : triggeringGeofences)
        {
            triggeringGeofencesIdsList.add(geofence.getRequestId());
        }
        String triggeringGeofencesIdsString = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            triggeringGeofencesIdsString = String.join(getString(R.string.symbolComma), triggeringGeofencesIdsList);
        }

        return geofenceTransitionString + getString(R.string.colon) + triggeringGeofencesIdsString;
    }

    private String getTransitionString(int transitionType) {
        Log.i(TAG, "getTransitionString");
        switch (transitionType) {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                return getString(R.string.geofenceTransitionEntered);
            case Geofence.GEOFENCE_TRANSITION_DWELL:
                return getString(R.string.geofenceTransitionDwell);
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                return getString(R.string.geofenceTransitionExited);
            default:
                return getString(R.string.unknownGeofenceTransition);
        }
    }


    private static void sendMessageToActivity(Context context, String msg) {
        Intent intent = new Intent(GEOFENCE_UPDATES);
        intent.putExtra(context.getString(R.string.zone), msg);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private static void sendRemoveMessageToActivity(Context context, String msg) {
        Intent intent = new Intent(GEOFENCE_REMOVE);
        intent.putExtra(context.getString(R.string.removeStations), msg);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private static void sendNotificationMessageToActivity(Context context, String msg) {
        Intent intent = new Intent(GEOFENCE_ON_ENTER);
        intent.putExtra(context.getString(R.string.createNotification), msg);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private static void sendExitMessageToActivity(Context context, String msg) {
        Intent intent = new Intent(GEOFENCE_ON_EXIT);
        intent.putExtra(context.getString(R.string.upperCaseExit), msg);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

}