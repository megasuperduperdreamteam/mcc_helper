package dreamteam.mcc_helper;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dreamteam.mcc_helper.mcc_parser.Timetable;
import dreamteam.mcc_helper.mcc_parser.Train_time;

/**
 *  SubClasses
 *  Class with supporting functions
 */

public class SubClasses {


    public static Time get_current_time() {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        Date now = new Date();
        String time = format.format(now); // format to wall time loosing current date
        try {
            now = format.parse(time); // reparse wall time
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Time(now.getTime());
    }

    public static String cast_time_to_str(Time time){
        DateFormat formatter = new SimpleDateFormat("HH:mm");
        return formatter.format(time);
    }


    public static Time cast_str_to_time(String str) throws ParseException {
        DateFormat formatter = new SimpleDateFormat("HH:mm");
        return (Time) formatter.parse(str);
    }

    public static List<Train_time> get_closest_timetable_time(Timetable timetable){
        List<Train_time> buffer = new ArrayList<>();
        for(Time time:timetable.getTable().keySet()){
            if (time.after(get_current_time())){
                if (!buffer.isEmpty()){
                    buffer.add(timetable.getTable().get(time));
                    return buffer;
                }
                buffer.add(timetable.getTable().get(time));
            }
        }
        return buffer;
    }


}
