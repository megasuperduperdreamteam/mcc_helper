package dreamteam.mcc_helper;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


import dreamteam.mcc_helper.mcc_adapter.ClockwiseTimetableTab;
import dreamteam.mcc_helper.mcc_adapter.CounterClockwiseTimetableTab;
import dreamteam.mcc_helper.mcc_adapter.PagerAdapter;
import dreamteam.mcc_helper.mcc_parser.AsyncParser;
import dreamteam.mcc_helper.mcc_parser.Schedule;
import dreamteam.mcc_helper.mcc_parser.Timetable;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 *  Station Activity
 */

public class StationActivity extends AppCompatActivity {
    
    TextView time;
    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;
    Timetable clockwise;
    Timetable counterclockwise;
    Schedule schedule;
    NotificationManager notificationManager;
    String TAG = StationActivity.class.getSimpleName();
    MyHandler handler;


    private ProgressBar progress;
    private Context context;

    public boolean networkStatus;

    // Start point
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        tabLayout = (TabLayout) findViewById(R.id.direction_tab);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        progress = (ProgressBar) findViewById(R.id.progress_bar);
        progress.setVisibility(View.VISIBLE);

        handler = new MyHandler(this);
        new Thread()
        {
            public void run()
            {
                networkStatus = isOnline();
                setSchedule();
                handler.sendEmptyMessage(1);
            }

        }.start();

    }

    // Start async parsing official mcc schedule website
    private void setSchedule(){
        Intent intent = getIntent();
        String stationName = null;

        try {
            stationName = intent.getStringExtra(getString(R.string.stationName));
            Log.i(TAG, "Get stationName ->" + String.valueOf(stationName));
        } catch (Exception e) {
            Log.e(TAG, stationName + " EXCEPTION");
            e.printStackTrace();
        }

        if (stationName!=null) {

            if (networkStatus) {

                Log.i(TAG, "Start AsyncParser");

                AsyncParser mt = new AsyncParser(getApplicationContext(), stationName);
                mt.execute();

                try {
                    schedule = (Schedule) mt.get();
                    clockwise = schedule.getClockwise_timetable();
                    counterclockwise = schedule.getCounterclockwise_timetable();

                    Log.i(TAG, "Get schedule -> " + schedule.toString());
                    Log.i(TAG, "Get clockwise -> " + clockwise.toString());
                    Log.i(TAG, "Get counterclockwise -> " + counterclockwise.toString());

                } catch (InterruptedException e) {
                    Log.e(TAG, stationName + " InterruptedException");

                    e.printStackTrace();
                    onBackPressed();
                } catch (ExecutionException e) {
                    Log.i(TAG, stationName + " ExecutionException");
                    e.printStackTrace();
                    onBackPressed();
                } catch (NullPointerException e) {
                    Log.i(TAG, stationName + " NUL_POINTER_EXCEPTION");
                    errorNotification(stationName);
                    onBackPressed();
                }



            } else {
                errorNotification(stationName);
                onBackPressed();
            }
        }else{
            Log.i(TAG, "StationName == null");
            clockwise = intent.getParcelableExtra(getString(R.string.timetableClockwise));
            counterclockwise = intent.getParcelableExtra(getString(R.string.timetableCounterclockwise));

        }


    }

    CounterClockwiseTimetableTab counterClockwiseTimetableTab;
    ClockwiseTimetableTab clockwiseTimetableTab;

    // Set data into View Pager
    private void setupViewPager(ViewPager viewPager) {
        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        clockwiseTimetableTab = new ClockwiseTimetableTab();
        clockwiseTimetableTab.setTimetable(clockwise, getApplicationContext());
        counterClockwiseTimetableTab = new CounterClockwiseTimetableTab();
        counterClockwiseTimetableTab.setTimetable(counterclockwise, getApplicationContext());

        pagerAdapter.addFragment(clockwiseTimetableTab, getString(R.string.clockwise));
        pagerAdapter.addFragment(counterClockwiseTimetableTab, getString(R.string.counterClockwise));
        viewPager.setAdapter(pagerAdapter);
    }

    // Internet connection checking
    public boolean isOnline() {
        Log.i(TAG, "Start checking network status");
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        Log.i(TAG, "Network status is " + netInfo);
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    // Error notification
    public void  errorNotification(String stationName) {

        Intent buttonIntent = new Intent(Constants.NOTIFICATION_BUTTON);
        buttonIntent.putExtra(context.getString(R.string.retry), stationName);
        LocalBroadcastManager.getInstance(this).sendBroadcast(buttonIntent);

        PendingIntent replyPendingIntent =
                PendingIntent.getBroadcast(this,0,buttonIntent,0);

        Notification.Builder notifBuilder = new Notification.Builder(getApplicationContext(), Constants.NOTIFICATION_CHANEL_STRING_ID)
                .setSmallIcon(R.drawable.ic_add2)
                .setContentTitle(getString(R.string.errorTitle))
                .setContentText(getString(R.string.errorTextMessage))
                .setColor(72626)
                .setContentIntent(replyPendingIntent)
                .setAutoCancel(true);
        Notification notification = notifBuilder.build();
        notificationManager.notify(Constants.STATION_NOTIFICATION_CHANNEL_ID, notification);
    }

    // Scrolling schedule to actual rows
    void tryToScroll(){
        final ScrollView clockwise_sv = (ScrollView) findViewById(R.id.scroll_clockwise);
        final ScrollView counter_sv = (ScrollView) findViewById(R.id.scroll_counterclockwise);
        final TableLayout clockwise_table = (TableLayout) findViewById(R.id.clockwise_table);
        TableLayout counter_table = (TableLayout) findViewById(R.id.counterclockwise_table);

        final TableRow wise_child = (TableRow) clockwise_table.getChildAt(clockwiseTimetableTab.scrollRow);
        final TableRow wise_child_next = (TableRow) clockwise_table.getChildAt(clockwiseTimetableTab.scrollRow+1);
        final TableRow counter_child = (TableRow) counter_table.getChildAt(counterClockwiseTimetableTab.scrollRow);
        final TableRow counter_child_next = (TableRow) counter_table.getChildAt(counterClockwiseTimetableTab.scrollRow+1);
        wise_child.setBackgroundColor(Color.rgb(0,250,0));
        wise_child_next.setBackgroundColor(Color.rgb(250,250,0));
        counter_child.setBackgroundColor(Color.rgb(0,250,0));
        counter_child_next.setBackgroundColor(Color.rgb(250,250,0));
        clockwise_sv.post(new Runnable() {
            public void run() {
                clockwise_sv.smoothScrollTo(0, wise_child.getTop());
                counter_sv.smoothScrollTo(0, counter_child.getTop());
            }
        });

    }

    private static class MyHandler extends Handler {
        private final WeakReference<StationActivity> mActivity;

        MyHandler(StationActivity activity) {
            mActivity = new WeakReference<StationActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            StationActivity activity = mActivity.get();
            if (activity != null) {

                activity.setupViewPager(activity.viewPager);
                activity.tabLayout.setupWithViewPager(activity.viewPager);
                activity.progress.setVisibility(View.GONE);
                activity.tryToScroll();
            }

        }
    }

    private final MyHandler mHandler = new MyHandler(this);

}
